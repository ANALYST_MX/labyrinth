#include "stdafx.h"
#include "Cell.hxx"

int Cell::count = 0;

Cell::Cell(const size_t x, const size_t y, const CellState state)
	: m_x(x), m_y(y), m_state(state)
{
	++count;
}

Cell::~Cell()
{
	--count;
}

void Cell::setX(const size_t x)
{
	m_x = x;
}

void Cell::setY(const size_t y)
{
	m_y = y;
}

void Cell::setState(const CellState state)
{
	m_state = state;
}

size_t Cell::getX() const
{
	return m_x;
}

size_t Cell::getY() const
{
	return m_y;
}

CellState Cell::getState() const
{
	return m_state;
}