// Labyrinth.cpp: ���������� ����� ����� ��� ����������.
//

#include "stdafx.h"
#include "Labyrinth.hxx"

const size_t MAX_LOADSTRING{ 100U };

const size_t MAX_EVENT{ 4 };
const size_t LABYRINTH_HEIGHT{ 8U };
const size_t LABYRINTH_WIGHT{ 12U };
const size_t SQUARE_SIDE{ 50 };

CellState LABYRINTH[LABYRINTH_HEIGHT][LABYRINTH_WIGHT]
{
	{ CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK },
	{ CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_FREE, CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_FREE, CellState::CELL_FREE, CellState::CELL_FREE, CellState::CELL_FREE, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK },
	{ CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_BRICK },
	{ CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_FREE, CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_FREE, CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_BRICK },
	{ CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_FREE, CellState::CELL_FREE, CellState::CELL_FREE, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_FREE, CellState::CELL_FREE, CellState::CELL_BRICK },
	{ CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_FREE, CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_FREE, CellState::CELL_BRICK },
	{ CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_FREE, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_FREE, CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_BRICK },
	{ CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_BRICK, CellState::CELL_FREE, CellState::CELL_BRICK }
};

size_t CURRENT_X{ 4 };
size_t CURRENT_Y{ 4 };

Cell* PATH[LABYRINTH_HEIGHT * LABYRINTH_WIGHT * MAX_EVENT];
size_t NEXT_PATH_INDEX{};

bool findPath(Cell* currentCell)
{
	bool exitFounded{ false };
	LABYRINTH[currentCell->getY()][currentCell->getX()] = CellState::CELL_FOUND;
	currentCell->setState(CellState::CELL_FOUND);

	PATH[NEXT_PATH_INDEX++] = new (std::nothrow) Cell(currentCell->getX(), currentCell->getY(), CellState::CELL_FOUND);
	if (currentCell->getX() == 0 || currentCell->getX() == LABYRINTH_WIGHT - 1 ||
		currentCell->getY() == 0 || currentCell->getY() == LABYRINTH_HEIGHT - 1)
	{
		exitFounded = true;
		return exitFounded;
	}

	static int dx[MAX_EVENT]{ 0, -1, 0, 1 };
	static int dy[MAX_EVENT]{ 1, 0, -1, 0 };
	for (size_t i{ 0 }; i < MAX_EVENT; ++i)
	{
		size_t x{ currentCell->getX() + dx[i] }, y{ currentCell->getY() + dy[i] };
		if (LABYRINTH[y][x] == CellState::CELL_BRICK || LABYRINTH[y][x] == CellState::CELL_PROCESSED)
			continue;
		if (LABYRINTH[y][x] == CellState::CELL_FREE)
		{
			Cell* neightbourCell = new (std::nothrow) Cell{ x, y };
			exitFounded = findPath(neightbourCell);
			delete neightbourCell;
			neightbourCell = nullptr;

			if (exitFounded)
			{
				break;
			}
			else
			{
				PATH[NEXT_PATH_INDEX - 1]->setState(CellState::CELL_PROCESSED);
				LABYRINTH[y][x] = CellState::CELL_PROCESSED;
				PATH[NEXT_PATH_INDEX++] = new (std::nothrow) Cell(currentCell->getX(), currentCell->getY(), CellState::CELL_FOUND);
			}
		}

	}

	return (exitFounded) ? true : false;
}

// ���������� ����������:
HINSTANCE hInst;                                // ������� ���������
WCHAR szTitle[MAX_LOADSTRING];                  // ����� ������ ���������
WCHAR szWindowClass[MAX_LOADSTRING];            // ��� ������ �������� ����

// ��������� ���������� �������, ���������� � ���� ������ ����:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LONG_PTR CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR  CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: ���������� ��� �����.

	// ������������� ���������� �����
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_LABYRINTH, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// ��������� ������������� ����������:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_LABYRINTH));

	MSG msg;

	// ���� ��������� ���������:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}

//
//  �������: MyRegisterClass()
//
//  ����������: ������������ ����� ����.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_LABYRINTH));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_LABYRINTH);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   �������: InitInstance(HINSTANCE, int)
//
//   ����������: ��������� ��������� ���������� � ������� ������� ����.
//
//   �����������:
//
//        � ������ ������� ���������� ���������� ����������� � ���������� ����������, � �����
//        ��������� � ��������� �� ����� ������� ���� ���������.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	RECTS WindowSize{ CW_USEDEFAULT, 0, 700, 600 };
	hInst = hInstance; // ��������� ���������� ���������� � ���������� ����������

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		WindowSize.left, WindowSize.top, WindowSize.right, WindowSize.bottom, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  �������: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ����������:  ������������ ��������� � ������� ����.
//
//  WM_COMMAND � ���������� ���� ����������
//  WM_PAINT � ���������� ������� ����
//  WM_DESTROY � ��������� ��������� � ������ � ���������
//
//
LONG_PTR CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	const unsigned int ID_TIMER{ 5555 };
	const unsigned int TIME_ELAPSE{ 200 };
	int wmId{};
	int wmEvent{};
	PAINTSTRUCT ps{};
	HDC hdc{};
	int xPos;
	int yPos;
	static HBITMAP g_hbmWall;
	static BITMAP g_bmWall;
	static HBITMAP g_hbmCat;
	static BITMAP g_bmCat;

	switch (message)
	{
	case WM_CREATE:
	{
		g_hbmWall = LoadBitmap(GetModuleHandleW(nullptr), MAKEINTRESOURCEW(IDB_WALL));
		if (!g_hbmWall)
			MessageBoxW(hWnd, L"������ � ��������� �� ����������", L"������!", MB_OK | MB_ICONEXCLAMATION);
		else
			GetObject(g_hbmWall, sizeof(g_bmWall), &g_bmWall);
		g_hbmCat = LoadBitmap(GetModuleHandleW(nullptr), MAKEINTRESOURCEW(IDB_CAT));
		if (!g_hbmCat)
			MessageBoxW(hWnd, L"������ �� ��������� �� ����������", L"������!", MB_OK | MB_ICONEXCLAMATION);
		else
			GetObject(g_hbmCat, sizeof(g_bmCat), &g_bmCat);
		Cell* c = new Cell{ CURRENT_X, CURRENT_Y, CellState::CELL_FREE };
		findPath(c);
		delete c;
		for (size_t y{}; y < LABYRINTH_HEIGHT; ++y)
			for (size_t x{}; x < LABYRINTH_WIGHT; ++x)
				if (LABYRINTH[y][x] != CellState::CELL_BRICK)
					LABYRINTH[y][x] = CellState::CELL_FREE;

		SetTimer(hWnd, ID_TIMER, TIME_ELAPSE, nullptr);
	}
	break;
	case WM_TIMER:
	{
		if (wParam == ID_TIMER)
		{
			static size_t stepNumber{};
			if (stepNumber < NEXT_PATH_INDEX)
			{
				CURRENT_X = PATH[stepNumber]->getX();
				CURRENT_Y = PATH[stepNumber]->getY();
				LABYRINTH[CURRENT_Y][CURRENT_X] = PATH[stepNumber]->getState();

				++stepNumber;
				InvalidateRect(hWnd, nullptr, TRUE);
			}
		}
	}
	break;
	case WM_LBUTTONDOWN:
	{
		xPos = GET_X_LPARAM(lParam);
		yPos = GET_Y_LPARAM(lParam);
		wchar_t buffer[MAX_LOADSTRING];
		swprintf(buffer, MAX_LOADSTRING, L"�� ������ ����� ������� ���� � ����� (%d, %d)\r\n��������� �������: %d", xPos, yPos, Cell::count);

		MessageBoxW(hWnd, buffer, L"������� ����", MB_OK);
	}
	break;
	case WM_COMMAND:
	{
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// ��������� ����� � ����:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		// TODO: �������� ���� ����� ��� ����������, ������������ HDC...
		HDC hdcWallMem = CreateCompatibleDC(hdc);
		HDC hdcCatMem = CreateCompatibleDC(hdc);
		HBITMAP hbmWallOld = (HBITMAP)SelectObject(hdcWallMem, g_hbmWall);
		HBITMAP hbmCatOld = (HBITMAP)SelectObject(hdcCatMem, g_hbmCat);
		HBRUSH foundCellBrush = CreateSolidBrush(RGB(200, 200, 200));
		HBRUSH processedCellBrush = CreateSolidBrush(RGB(50, 50, 50));
		HBRUSH oldBrush;

		for (size_t y{}; y < LABYRINTH_HEIGHT; ++y)
			for (size_t x{}; x < LABYRINTH_WIGHT; ++x)
			{
				if (x == CURRENT_X && y == CURRENT_Y)
					BitBlt(hdc, (1 + (int)x)*SQUARE_SIDE, (1 + (int)y)*SQUARE_SIDE, g_bmCat.bmWidth, g_bmCat.bmHeight, hdcCatMem, 0, 0, SRCCOPY);
				else if (LABYRINTH[y][x] == CellState::CELL_BRICK)
					BitBlt(hdc, (1 + (int)x)*SQUARE_SIDE, (1 + (int)y)*SQUARE_SIDE, g_bmWall.bmWidth, g_bmWall.bmHeight, hdcWallMem, 0, 0, SRCCOPY);
				else if (LABYRINTH[y][x] == CellState::CELL_FOUND)
				{
					oldBrush = (HBRUSH)SelectObject(hdc, foundCellBrush);
					Rectangle(hdc, (1 + (int)x)*SQUARE_SIDE, (1 + (int)y)*SQUARE_SIDE, (2 + (int)x)*SQUARE_SIDE, (2 + (int)y)*SQUARE_SIDE);
					SelectObject(hdc, oldBrush);
				}
				else if (LABYRINTH[y][x] == CellState::CELL_PROCESSED)
				{
					oldBrush = (HBRUSH)SelectObject(hdc, processedCellBrush);
					Rectangle(hdc, (1 + (int)x)*SQUARE_SIDE, (1 + (int)y)*SQUARE_SIDE, (2 + (int)x)*SQUARE_SIDE, (2 + (int)y)*SQUARE_SIDE);
					SelectObject(hdc, oldBrush);
				}
			}

		SelectObject(hdcWallMem, hbmWallOld);
		SelectObject(hdcCatMem, hbmCatOld);

		DeleteObject(foundCellBrush);
		DeleteObject(processedCellBrush);

		DeleteDC(hdcWallMem);
		DeleteDC(hdcCatMem);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		DeleteObject(g_hbmWall);
		DeleteObject(g_hbmCat);

		for (Cell* c : PATH)
			delete c;
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// ���������� ��������� ��� ���� "� ���������".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
