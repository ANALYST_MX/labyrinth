#pragma once

enum class CellState : std::int8_t { CELL_FREE = 0, CELL_BRICK = 1, CELL_FOUND = 2, CELL_PROCESSED = 3 };

class Cell
{
	size_t m_x{};
	size_t m_y{};
	CellState m_state{};
public:
	Cell() = default;
	Cell(const size_t, const size_t, const CellState = CellState::CELL_FREE);
	virtual ~Cell();
	void setX(const size_t);
	void setY(const size_t);
	void setState(const CellState);
	size_t getX() const;
	size_t getY() const;
	CellState getState() const;
	static int count;

};

