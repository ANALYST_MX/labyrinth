#pragma once

#include "resource.h"
#include "Cell.hxx"


typedef struct _RECTS       /* rcs */
{
	int     left;
	int     top;
	int     right;
	int     bottom;
} RECTS, *PRECTS, *LPRECTS;

extern const size_t MAX_LOADSTRING;
extern const size_t LABYRINTH_HEIGHT;
extern const size_t LABYRINTH_WIGHT;
extern const size_t SQUARE_SIDE;